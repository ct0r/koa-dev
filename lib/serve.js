const http = require('http')
const Koa = require('koa')

const serve = async (middleware, host, port) => {
  const app = new Koa()
    .use(middleware)

  return new Promise((resolve, reject) => {
    http.createServer(app.callback())
      .on('error', reject)
      .listen(3000, resolve)
  })
}

module.exports = serve
