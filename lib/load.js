module.exports = async file => {
  try {
    const middleware = await require(file)

    if (typeof middleware !== 'function') {
      throw TypeError(`The file "${file}" does not export a function`)
    }

    return middleware
  } catch (err) {
    return Promise.reject(err)
  }
}
