const fs = require('fs')
const path = require('path')
const mri = require('mri')
const dotEnv = require('dotenv')
const load = require('../lib/load')
const serve = require('../lib/serve')
const { version } = require('../package')

const help = `
Usage: koa-dev [options] [path]

Options:
  -d, --dotenv      Specify alternate .env file (default .env)
  -h, --help        Print usage
  -H, --host        Specify host on which koa will run
  -p, --port        Specify port to listen on (default 3000)
  -v, --version     Print version
`

const exit = (message, code = 0) => {
  console.log(message)
  process.exit(code)
}

const flags = mri(process.argv.slice(2), {
  default: {
    host: '::',
    port: 3000,
    dotenv: '.env'
  },
  alias: {
    d: 'dotenv',
    h: 'help',
    H: 'host',
    p: 'port',
    v: 'version'
  },
  unknown: flag => exit(`Unknown flag: ${flag}\n${help}`, 1)
})

if (flags.help) exit(help)
if (flags.version) exit(version)

dotEnv.config({
  path: path.resolve(process.cwd(), flags.dotenv)
})

let [ file ] = flags._

if (!file) {
  try {
    file = require(path.resolve(process.cwd(), 'package.json')).main || 'index.js'
  } catch (err) {
    if (err.code !== 'MODULE_NOT_FOUND') {
      exit(`Could not read "package.json": ${err.message}`, 1)
    }
  }
}

if (!file) exit('No path defined', 1)

file = file[0] !== '/'
  ? path.resolve(process.cwd(), file)
  : file

if (!fs.existsSync(file)) {
  exit(`Could not find "${path.basename(file)}"`, 1)
}

load(file)
  .then(middleware => serve(middleware, flags.host, flags.port))
  .then(_ => console.log(`Listening on port: ${flags.port}`))
  .catch(console.log)
